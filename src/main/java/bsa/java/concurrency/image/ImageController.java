package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    ImageService imageService;

    Logger logger = LoggerFactory.getLogger(ImageController.class);

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        logger.info("POST request, CLASS:ImageController , METHOD: batchUploadImages()");
        imageService.saveFile(files);
        return new ResponseEntity<>("Images saved", HttpStatus.OK);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> searchMatches(@RequestParam("image") MultipartFile file, @RequestParam(value = "threshold",
            defaultValue = "0.9") double threshold) {
        logger.info("POST request, CLASS:ImageController , METHOD: searchMatches()");
        List<SearchResultDTO> imageList = imageService.findImages(file, threshold);
        if (imageList.isEmpty()) {
            return new ResponseEntity<>("Image saved", HttpStatus.OK);
        }
        return new ResponseEntity<>(imageList, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteImage(@PathVariable("id") UUID imageId) {
        logger.info("DELETE request, CLASS:ImageController , METHOD: deleteImage(), ID: " + imageId);
        imageService.deleteImageById(imageId);
        return new ResponseEntity<>("Image " + imageId + " deleted", HttpStatus.OK);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> purgeImages() {
        logger.info("DELETE request, CLASS:ImageController , METHOD: purgeImages()");
        imageService.deleteAllImages();
        return new ResponseEntity<>("All images deleted", HttpStatus.OK);
    }
}
