package bsa.java.concurrency.image;

import bsa.java.concurrency.dhasher.HashService;
import bsa.java.concurrency.fs.FileService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class ImageService {
    @Autowired
    FileService fileService;

    @Autowired
    ImageRepository imageRepository;

    public void saveFile(MultipartFile[] files) {
        Arrays
                .stream(files)
                .map(file -> {
                    var path = fileService.saveFile(file);
                    var imageHash = HashService.calculateHash(fileService.getByteArray(file));
                    path.thenAccept(result -> imageRepository.save(new Image(UUID.randomUUID(), imageHash, getImageUrl(path))));
                    return path;
                }).forEach(CompletableFuture::join);
    }

    public String getImageUrl(CompletableFuture<String> completableFuture) {
        String url = "";
        try {
            url = completableFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return url;
    }

    public List<SearchResultDTO> findImages(MultipartFile file, double threshold) {
        Long hash = HashService.calculateHash(fileService.getByteArray(file));
        List<Image> images = imageRepository.findAll();
        List<SearchResultDTO> answer = new ArrayList<>();
        for (Image img : images) {
            double matchPercent = HashService.searchDifference(img.getHash(), hash);
            if (matchPercent >= threshold) {
                SearchResultDTO resultDTO = SearchResultDTO.fromEntity(img);
                resultDTO.setMatchPercent(matchPercent * 100);
                answer.add(resultDTO);
            }
        }
        if (answer.isEmpty()) {
            var future = fileService.saveFile(file);
            future.thenAccept(result -> imageRepository.save(new Image(UUID.randomUUID(), hash, getImageUrl(future))));
        }
        return answer;
    }

    @Transactional
    public void deleteImageById(UUID id) {
        fileService.deleteImageById(id);
        imageRepository.deleteByImageId(id);
    }

    @Transactional
    public void deleteAllImages() {
        fileService.deleteAllImages();
        imageRepository.deleteAll();
    }
}
