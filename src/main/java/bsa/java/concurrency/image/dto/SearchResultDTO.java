package bsa.java.concurrency.image.dto;

import bsa.java.concurrency.image.Image;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class SearchResultDTO {
    UUID imageId;
    double matchPercent;
    String imageUrl;

    public static SearchResultDTO fromEntity(Image image) {
        return SearchResultDTO
                .builder()
                .imageId(image.getImageId())
                .imageUrl(image.getImageUrl())
                .build();
    }
}
