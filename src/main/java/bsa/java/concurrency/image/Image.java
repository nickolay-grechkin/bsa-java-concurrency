package bsa.java.concurrency.image;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "images")
public class Image {
    @Column
    private UUID imageId;

    @Id
    @Column(name = "hash", updatable = false, nullable = false)
    private long hash;

    @Column
    private String imageUrl;
}
