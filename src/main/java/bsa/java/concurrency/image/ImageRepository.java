package bsa.java.concurrency.image;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {
    Optional<Image> findAllByImageId(UUID id);

    void deleteByImageId(UUID id);
}
