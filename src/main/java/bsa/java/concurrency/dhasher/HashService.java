package bsa.java.concurrency.dhasher;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

@Service
public class HashService {
    public static long calculateHash(byte[] image) {
        try {
            var img = ImageIO.read(new ByteArrayInputStream(image));
            return calculateDHash(preprocessImage(img));
        } catch (Exception err) {
            throw new RuntimeException(err.getMessage());
        }
    }

    private static BufferedImage preprocessImage(BufferedImage image) {
        var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var output = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);
        return output;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (var row = 1; row < 9; row++) {
            for (var col = 1; col < 9; col++) {
                var prev = brightnessScore(processedImage.getRGB(col - 1, row));
                var current = brightnessScore(processedImage.getRGB(col, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }

        return hash;
    }

    public static double hamingDistance(String x1, String x2) {
        int index = 0;
        for (int i = 0; i < x1.length(); i++) {
            var diff = Long.parseLong(String.valueOf(x1.charAt(i))) ^ Long.parseLong(String.valueOf(x2.charAt(i)));
            if (diff == 1) {
                index++;
            }
        }
        return 1 - (index / 64.0);
    }

    public static double searchDifference(Long hash1, Long hash2) {
        String binaryHash1 = Long.toBinaryString(hash1);
        String binaryHash2 = Long.toBinaryString(hash2);
        return hamingDistance(binaryHash1, binaryHash2);
    }
}
