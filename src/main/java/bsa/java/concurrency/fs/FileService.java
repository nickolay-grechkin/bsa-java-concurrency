package bsa.java.concurrency.fs;

import bsa.java.concurrency.image.Image;
import bsa.java.concurrency.image.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class FileService implements FileSystem {
    @Autowired
    ImageRepository imageRepository;
    public static final String PATH_TO_STORAGE = "D:\\bsa-java-concurrency\\src\\main\\webapp\\images";

    public String saveFileToDirectory(byte[] file, String filename) {
        Path pathToStorage = Paths.get(PATH_TO_STORAGE);
        if (!Files.exists(pathToStorage)) {
            try {
                Files.createDirectories(pathToStorage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(pathToStorage + "\\" + filename));
            bos.write(file);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "http://localhost:7777/images/" + filename;
    }

    @Override
    public CompletableFuture<String> saveFile(MultipartFile file) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        return CompletableFuture.supplyAsync(() -> saveFileToDirectory(getByteArray(file), file.getOriginalFilename()), executorService);
    }

    public byte[] getByteArray(MultipartFile file) {
        byte[] array = new byte[0];
        try {
            array = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    public void deleteImageById(UUID id) {
        Optional<Image> image = imageRepository.findAllByImageId(id);
        if (image.isPresent()) {
            String fileName = image.get().getImageUrl();
            fileName = fileName.substring(fileName.lastIndexOf("/"));
            File file = new File(PATH_TO_STORAGE + "\\" + fileName);
            file.delete();
        }
    }

    public void deleteAllImages() {
        File fileStorage = new File(PATH_TO_STORAGE);
        for (File file : Objects.requireNonNull(fileStorage.listFiles())) {
            file.delete();
        }
    }
}
